package com.rakuten.product.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rakuten.product.model.Category;
import com.rakuten.product.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.math.BigDecimal;
import java.util.List;

public class TestUtils {

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> Page<T> page(List<T> list) {
        return new PageImpl(list);
    }

    /**
     * Create a test Product.
     *
     * @return Dummy Product.
     */
    public static Product getProduct() {
        return Product.builder()
                .id(1l)
                .name("Product")
                .description("Dummy product")
                .price(BigDecimal.valueOf(1))
                .category(getCategory())
                .build();
    }

    /**
     * Create a test Category.
     *
     * @return Dummy Category.
     */
    public static Category getCategory() {
        return Category.builder().id(1l).name("Mobiles").description("All the mobile phones.").build();
    }
}
