package com.rakuten.product.repository;

import com.rakuten.product.model.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
@SpringBootTest
public class CategoryRepositoryTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CategoryRepository categoryRepository;


    @Test
    public void save() {
        Category category = getTestCategory();
        Category savedCategory = categoryRepository.save(category);
        entityManager.flush();

        String categoryName = jdbcTemplate.queryForObject("select name from category where id = ? ", String.class,
                savedCategory.getId());
        assertEquals(category.getName(), categoryName);
    }

    @Test
    public void saveUpdatedCategory() {
        Category category = getTestCategory();
        Category savedCategory = categoryRepository.save(category);
        String updateName = "New Mobile Category";
        savedCategory.setName(updateName);
        categoryRepository.save(savedCategory);
        entityManager.flush();

        String customerName = jdbcTemplate.queryForObject("select name from category where id = ? ", String.class,
                savedCategory.getId());
        assertEquals(updateName, customerName);
    }


    @Test
    public void findChildCategoriesByParentCategoryID() {
        Category root = getTestCategory();
        root.setName("Root");

        root = categoryRepository.save(root);

        Category firstChild = getTestCategory();
        firstChild.setName("First child");
        firstChild.setParentCategory(root);
        categoryRepository.save(firstChild);

        Category secondtChild = getTestCategory();
        secondtChild.setName("Second child");
        secondtChild.setParentCategory(root);
        categoryRepository.save(secondtChild);

        List<Category> childCategories = categoryRepository.findChildCategoriesByparentCategoryId(root.getId(),
                null).getContent();
        childCategories.forEach(c -> System.out.println("Child name : " + c.getName()));
        assertEquals(2, childCategories.size());
    }


    private Category getTestCategory() {
        return Category.builder().name("Mobiles").description("All the mobile phones.").build();
    }
}