package com.rakuten.product.repository;

import com.rakuten.product.model.Category;
import com.rakuten.product.model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Rollback
@SpringBootTest
public class ProductRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void findAll() {
        final int numberOfObjects = 3;
        LongStream.range(0, numberOfObjects)
                .forEach(
                        i -> productRepository.save(getTestProduct())
                );
        entityManager.flush();

        List<Product> productList = new ArrayList<Product>();
        productRepository.findAll().forEach(product -> productList.add(product));

        assertEquals(numberOfObjects, productList.size());
        productRepository.deleteAll();
    }

    @Test
    public void save() {
        Product savedProduct = productRepository.save(getTestProduct());
        entityManager.flush();
        String productName = jdbcTemplate.queryForObject("select name from product where id = ? ", String.class,
                savedProduct.getId());
        assertEquals(savedProduct.getName(), productName);
        productRepository.deleteAll();

    }

    @Test
    public void findProductsByCategoryId() {
        Category category = Category.builder()
                .name("Mobiles")
                .description("Mobile Category")
                .build();
        category = categoryRepository.save(category);

        Product product = getTestProduct();
        product.setCategory(category);
        product = productRepository.save(product);

        List<Product> productList = productRepository.findProductsByCategoryId(category.getId(), null).getContent();

        assertEquals(product.getName(), productList.get(0).getName());
        productRepository.deleteAll();

    }

    private Product getTestProduct() {
        return Product.builder()
                .name("Product")
                .description("Dummy product")
                .price(BigDecimal.valueOf(1))
                .build();
    }
}