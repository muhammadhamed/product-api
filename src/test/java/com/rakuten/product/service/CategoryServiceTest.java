package com.rakuten.product.service;

import com.rakuten.product.model.Category;
import com.rakuten.product.repository.CategoryRepository;
import com.rakuten.product.util.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {
    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryService categoryService;

    private Pageable pageable = PageRequest.of(0, 10);

    @Test
    public void getCategoriesByCategoryId() {
        List<Category> categories = Arrays.asList(TestUtils.getCategory());
        when(categoryRepository.findChildCategoriesByparentCategoryId(any(), any())).thenReturn(TestUtils.page(categories));

        List<Category> returnedCategories = categoryService.getCategories(1l, pageable).getContent();
        assertEquals(categories, returnedCategories);
    }

    @Test
    public void getCategories() {
        List<Category> categories = Arrays.asList(TestUtils.getCategory());
        when(categoryRepository.findAll(pageable)).thenReturn(TestUtils.page(categories));

        List<Category> returnedCategories = categoryService.getCategories(pageable).getContent();
        assertEquals(categories, returnedCategories);
    }

    @Test
    public void createCategory() {
        Category category = TestUtils.getCategory();
        when(categoryRepository.save(any())).thenReturn(category);

        Category saveCategory = categoryService.createCategory(category);
        assertEquals(category, saveCategory);
    }

    @Test
    public void updateCategory() {
        Category category = TestUtils.getCategory();
        when(categoryRepository.findById(any())).thenReturn(Optional.of(category));
        when(categoryRepository.save(any())).thenReturn(category);

        category = categoryRepository.findById(1l).get();
        category.setName("Changed");
        Category saveCategory = categoryService.updateCategory(category);
        assertEquals(category, saveCategory);
    }

    @Test
    public void getCategory() {
        Category category = TestUtils.getCategory();
        when(categoryRepository.findById(any())).thenReturn(Optional.of(category));

        Category returnedCategory = categoryService.getCategory(1l).get();
        assertEquals(category, returnedCategory);
    }


}