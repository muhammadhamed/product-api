package com.rakuten.product.service;

import com.rakuten.product.model.Product;
import com.rakuten.product.repository.ProductRepository;
import com.rakuten.product.util.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest

public class ProductServiceTest {

    private static Pageable pageable = PageRequest.of(0, 10);
    @Mock
    private ProductRepository productRepository;
    @Mock
    private CurrencyConverterService CurrencyConverterService;
    @InjectMocks
    private ProductService productService;

    @Test
    public void getProducts() {
        List<Product> products = Arrays.asList(TestUtils.getProduct());
        when(productRepository.findAll(pageable)).thenReturn(TestUtils.page(products));

        List<Product> returnedProducts = productService.getProducts(pageable).getContent();

        assertEquals(products, returnedProducts);
    }

    @Test
    public void getProductsByCatergoryId() {
        List<Product> products = Arrays.asList(TestUtils.getProduct());
        when(productRepository.findProductsByCategoryId(1l, pageable)).thenReturn(TestUtils.page(products));

        List<Product> returnedProducts = productService.getProductsByCatergoryId(1l, pageable).getContent();

        assertEquals(products, returnedProducts);
    }

    @Test
    public void getProduct() {
        Product product = TestUtils.getProduct();
        when(productRepository.findById(any())).thenReturn(Optional.of(product));

        Product returnProduct = productService.getProduct(1l).get();
        assertEquals(product, returnProduct);
    }

    @Test
    public void createProduct() {
        Product product = TestUtils.getProduct();
        product.setCurrencyCode(null);

        when(productRepository.save(any())).thenReturn(product);

        Product returnProduct = productService.createProduct(product);

        verify(productRepository).save(product);
        assertEquals(product, returnProduct);
    }

    @Test
    public void createProductWithCurrencyCode() {
        Product product = TestUtils.getProduct();
        product.setCurrencyCode("EGP");
        product.setPrice(BigDecimal.valueOf(20));

        when(productRepository.save(any())).thenReturn(product);
        when(CurrencyConverterService.convertToEuro(any(), any())).thenReturn(BigDecimal.valueOf(1));

        Product returnProduct = productService.createProduct(product);

        verify(productRepository).save(product);
        assertEquals(product, returnProduct);
    }

    @Test
    public void updateProduct() {
        Product product = TestUtils.getProduct();
        product.setCurrencyCode(null);

        when(productRepository.save(any())).thenReturn(product);

        Product returnProduct = productService.updateProduct(product);

        verify(productRepository).save(product);
        assertEquals(product, returnProduct);
    }

    @Test
    public void deleteProduct() {
        Product product = TestUtils.getProduct();
        product.setCurrencyCode(null);

        doNothing().when(productRepository).delete(any());

        productService.deleteProduct(product);
        verify(productRepository).delete(product);
    }


}