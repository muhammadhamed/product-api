package com.rakuten.product.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FixerCurrencyConverterServiceTest {

    @Autowired
    private CurrencyConverterService CurrencyConverterService;


    @Test
    public void convertToEuro() {
        BigDecimal euroAmount = CurrencyConverterService.convertToEuro("EGP", BigDecimal.valueOf(20));
        IntStream.range(1, 10).forEach(i -> CurrencyConverterService.convertToEuro("EGP", BigDecimal.valueOf(20)));
        System.out.println("euroAmount : " + euroAmount);
        assertTrue(euroAmount.doubleValue() > 1);
    }
}