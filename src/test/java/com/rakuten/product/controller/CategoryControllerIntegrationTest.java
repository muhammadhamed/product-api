package com.rakuten.product.controller;

import com.rakuten.product.ProductApplication;
import com.rakuten.product.model.Category;
import com.rakuten.product.repository.CategoryRepository;
import com.rakuten.product.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductApplication.class)
@AutoConfigureMockMvc
public class CategoryControllerIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private CategoryRepository repository;

    @Before
    public void setup() {
        repository.deleteAll();
    }

    @Test
    public void givenCategories_whenGetCategories_thenSuccessWithCategories() throws Exception {

        Category category = repository.save(TestUtils.getCategory());

        mvc.perform(get("/categories")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].name", is("Mobiles")));

        repository.delete(category);
    }

    @Test
    public void givenCategories_whenGetCategories_thenSuccessWithSortedCategories() throws Exception {
        Category bCategory = TestUtils.getCategory();
        bCategory.setId(null);
        bCategory.setName("B");
        Category aCategory = TestUtils.getCategory();
        aCategory.setId(null);
        aCategory.setName("A");

        bCategory = repository.save(bCategory);
        aCategory = repository.save(aCategory);


        mvc.perform(get("/categories")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].name", is("A")));

        repository.delete(bCategory);
        repository.delete(aCategory);
    }

}
