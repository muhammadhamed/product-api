package com.rakuten.product.controller;

import com.rakuten.product.dto.CreateProductDto;
import com.rakuten.product.dto.ProductDto;
import com.rakuten.product.model.Product;
import com.rakuten.product.service.ProductService;
import com.rakuten.product.util.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ModelMapper modelMapper;

    @MockBean
    private ProductService productService;


    @Test
    public void getProducts() throws Exception {
        when(productService.getProducts(any())).thenReturn(getTestProducts());
        mvc.perform(get("/products")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name").value("Product"));
    }

    @Test
    public void getProductsByCategory() throws Exception {
        when(productService.getProductsByCatergoryId(any(), any())).thenReturn(getTestProducts());
        mvc.perform(get("/products?categoryId=1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name").value("Product"));
    }

    @Test
    public void getProduct() throws Exception {
        when(productService.getProduct(any())).thenReturn(Optional.of(TestUtils.getProduct()));
        mvc.perform(get("/products/1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Product"));
    }

    @Test
    public void getProductNotFoundException() throws Exception {
        when(productService.getProduct(any())).thenReturn(Optional.ofNullable(null));
        mvc.perform(get("/products/1")).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteProduct() throws Exception {
        Product product = TestUtils.getProduct();
        when(productService.getProduct(any())).thenReturn(Optional.of(product));
        doNothing().when(productService).deleteProduct(any());
        mvc.perform(delete("/products/1")).andDo(print())
                .andExpect(status().isOk());
        verify(productService).deleteProduct(same(product));
    }

    @Test
    public void deleteProductNotFound() throws Exception {
        Product product = TestUtils.getProduct();
        when(productService.getProduct(any())).thenReturn(Optional.ofNullable(null));
        mvc.perform(delete("/products/1")).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void createProduct() throws Exception {
        Product product = TestUtils.getProduct();

        when(productService.createProduct(any())).thenReturn(product);
        mvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON).content(TestUtils.asJsonString(new CreateProductDto(product)))
        ).andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Product"));
    }

    @Test
    public void updateProduct() throws Exception {
        Product product = TestUtils.getProduct();
        when(productService.getProduct(any())).thenReturn(Optional.of(product));
        when(productService.updateProduct(any())).thenReturn(product);
        mvc.perform(
                put("/products").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.asJsonString(new ProductDto(product)))).andDo(print()
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Product"));
    }


    public Page<Product> getTestProducts() {
        return new PageImpl<Product>(Arrays.asList(TestUtils.getProduct()));
    }
}