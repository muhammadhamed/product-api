package com.rakuten.product.controller;

import com.rakuten.product.ProductApplication;
import com.rakuten.product.model.Category;
import com.rakuten.product.model.Product;
import com.rakuten.product.repository.CategoryRepository;
import com.rakuten.product.repository.ProductRepository;
import com.rakuten.product.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductApplication.class)
@AutoConfigureMockMvc
public class ProductControllerIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ProductRepository repository;

    @Autowired
    private CategoryRepository categoryRepository;


    @Before
    public void setup() {
        repository.deleteAll();
        categoryRepository.deleteAll();
    }

    @Test
    public void givenProducts_whenGetProducts_thenSuccessWithProducts() throws Exception {

        Product product = TestUtils.getProduct();
        Category category = categoryRepository.save(product.getCategory());
        product.setCategory(category);
        product = repository.save(product);

        mvc.perform(get("/products")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].name", is("Product")));

        repository.delete(product);
        categoryRepository.delete(category);
    }

    @Test
    public void givenProducts_whenGetProducts_thenSuccessWithSortedProductsBasedOnPrice() throws Exception {
        Category category = categoryRepository.save(TestUtils.getCategory());


        Product bProduct = TestUtils.getProduct();
        bProduct.setId(null);
        bProduct.setName("Product B");
        bProduct.setPrice(BigDecimal.valueOf(100.0));
        bProduct.setCategory(category);

        Product aProduct = TestUtils.getProduct();
        aProduct.setId(null);
        aProduct.setName("Product A");
        aProduct.setPrice(BigDecimal.valueOf(10.0));
        aProduct.setCategory(category);
        bProduct = repository.save(bProduct);
        aProduct = repository.save(aProduct);


        mvc.perform(get("/products?pageNumber=1&orderBy=price&direction=ASC")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].price", is(10.0)));

        repository.delete(bProduct);
        repository.delete(aProduct);
        categoryRepository.delete(category);
    }

}
