package com.rakuten.product.controller;

import com.rakuten.product.dto.CategoryDto;
import com.rakuten.product.dto.CreateCategoryDto;
import com.rakuten.product.model.Category;
import com.rakuten.product.service.CategoryService;
import com.rakuten.product.util.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CategoryService categoryService;

    @MockBean
    private ModelMapper modelMapper;

    @Test
    public void getCategories() throws Exception {
        when(modelMapper.map(any(), any())).thenCallRealMethod();
        when(categoryService.getCategories(any())).thenReturn(getTestCategories());
        mvc.perform(get("/categories")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name").value("Mobiles"));
    }

    @Test
    public void getCategoriesByParentCategory() throws Exception {
        when(modelMapper.map(any(), any())).thenCallRealMethod();
        when(categoryService.getCategories(any(), any())).thenReturn(getTestCategories());
        mvc.perform(get("/categories?parentCategoryId=1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name").value("Mobiles"));
    }

    @Test
    public void getCategoryById() throws Exception {
        when(categoryService.getCategory(any())).thenReturn(Optional.of(TestUtils.getCategory()));
        mvc.perform(get("/categories/1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Mobiles"));
    }

    @Test
    public void getCategoryByIdNotFound() throws Exception {
        when(categoryService.getCategory(any())).thenReturn(Optional.ofNullable(null));
        mvc.perform(get("/categories/1")).andDo(print())
                .andExpect(status().isNotFound());
        verify(categoryService).getCategory(same(1l));
    }

    @Test
    public void createCategory() throws Exception {
        Category category = TestUtils.getCategory();
        when(categoryService.createCategory(any())).thenReturn(category);
        mvc.perform(post("/categories").contentType(MediaType.APPLICATION_JSON).content(TestUtils.asJsonString(new CreateCategoryDto(category)))
        ).andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Mobiles"));
    }

    @Test
    public void updateCategory() throws Exception {
        Category category = TestUtils.getCategory();
        when(categoryService.getCategory(any())).thenReturn(Optional.of(category));
        when(categoryService.updateCategory(any())).thenReturn(category);
        mvc.perform(
                put("/categories").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.asJsonString(new CategoryDto(category))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Mobiles"));
    }

    public Page<Category> getTestCategories() {
        return new PageImpl<Category>(Arrays.asList(TestUtils.getCategory()));
    }

}