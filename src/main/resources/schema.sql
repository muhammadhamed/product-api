CREATE TABLE category (
    id BIGINT(16) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(500),
    parent_category BIGINT(16),
    PRIMARY KEY (id)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE product (
    id BIGINT(16) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(500) NOT NULL,
    price DECIMAL(6 , 4 ) NOT NULL,
    category_id BIGINT(16) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_category FOREIGN KEY (category_id)
        REFERENCES category (id)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;