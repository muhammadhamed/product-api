package com.rakuten.product.controller;

import com.rakuten.product.dto.CreateProductDto;
import com.rakuten.product.dto.ProductDto;
import com.rakuten.product.exception.ProductNotFountException;
import com.rakuten.product.model.Product;
import com.rakuten.product.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Api(value = "Product API")
@RequestMapping("/products")
@Slf4j
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class ProductController {

    private ProductService productService;

    private ModelMapper modelMapper;

    @ApiOperation("List All the products, or product under a specific category.")
    @GetMapping
    public ResponseEntity<Page<ProductDto>> getProducts(@RequestParam Optional<Long> categoryId,
                                                        @RequestParam(required = false, defaultValue = "0") int pageNumber,
                                                        @RequestParam(required = false, defaultValue = "10") int pageSize,
                                                        @RequestParam(required = false, defaultValue = "name") String orderBy,
                                                        @RequestParam(required = false, defaultValue = "ASC") String direction) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.Direction.valueOf(direction), orderBy);
        log.info("Requesting a products page :" + pageable);

        Page<ProductDto> productDtos;
        if (categoryId.isPresent()) {
            log.debug("Get The products under the Category with Id : " + categoryId);
            productDtos = productService.getProductsByCatergoryId(categoryId.get(), pageable).map(ProductDto::new);
        } else {
            log.debug("Get All the products.");
            productDtos = productService.getProducts(pageable).map(ProductDto::new);
        }
        return ResponseEntity.ok(productDtos);
    }

    @ApiOperation("Retrieve a specific product.")
    @GetMapping("/{productId}")
    public ResponseEntity<ProductDto> getProduct(@PathVariable Long productId) {
        Optional<ProductDto> optionalProduct = productService.getProduct(productId).map(ProductDto::new);
        if (optionalProduct.isPresent()) {
            return ResponseEntity.ok(optionalProduct.get());
        }
        log.debug("The product with Id : " + productId + " is missing.");
        throw new ProductNotFountException();
    }

    @ApiOperation("Delete a specific product.")
    @DeleteMapping("/{productId}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long productId) {
        Optional<Product> optionalProduct = productService.getProduct(productId);
        log.debug("Deleting a product with Id : " + productId);
        if (optionalProduct.isPresent()) {
            productService.deleteProduct(optionalProduct.get());
            return ResponseEntity.ok().build();
        }
        throw new ProductNotFountException();
    }

    @ApiOperation("Create a new product.")
    @PostMapping
    public ResponseEntity<ProductDto> createProduct(@Valid @RequestBody CreateProductDto productDto) {
        Product product = productService.createProduct(modelMapper.map(productDto, Product.class));
        log.debug("Create a new product with name : " + productDto.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(new ProductDto(product));
    }

    @ApiOperation("Update product characteristics.")
    @PutMapping
    public ResponseEntity<ProductDto> updateProduct(@Valid @RequestBody ProductDto productDto) {
        Optional<Product> optionalProduct = productService.getProduct(productDto.getId());
        log.debug("Updating the product with Id : " + productDto.getId());
        if (optionalProduct.isPresent()) {
            Product product = productService.updateProduct(modelMapper.map(productDto, Product.class));
            return ResponseEntity.ok(new ProductDto(product));
        }

        log.debug("The product with Id : " + productDto.getId() + " is missing.");
        throw new ProductNotFountException();
    }

}
