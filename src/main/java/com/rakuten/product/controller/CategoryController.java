package com.rakuten.product.controller;

import com.rakuten.product.dto.CategoryDto;
import com.rakuten.product.dto.CreateCategoryDto;
import com.rakuten.product.exception.CategoryNotFountException;
import com.rakuten.product.model.Category;
import com.rakuten.product.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Api(value = "Category API")
@RequestMapping("/categories")
@Slf4j
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class CategoryController {

    private CategoryService categoryService;

    private ModelMapper modelMapper;

    @ApiOperation("List All the Categories, or get Category children.")
    @GetMapping
    public ResponseEntity<Page<CategoryDto>> getCategories(@RequestParam Optional<Long> parentCategoryId,
                                                           @RequestParam(required = false, defaultValue = "0") int pageNumber,
                                                           @RequestParam(required = false, defaultValue = "10") int pageSize,
                                                           @RequestParam(required = false, defaultValue = "name") String orderBy,
                                                           @RequestParam(required = false, defaultValue = "ASC") String direction) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.Direction.valueOf(direction), orderBy);

        log.info("Requesting a Categories page :" + pageable);
        Page<CategoryDto> categoryDtoPage;
        if (parentCategoryId.isPresent()) {
            log.debug("Getting the categories by parentCategoryId : " + parentCategoryId);
            categoryDtoPage = categoryService.getCategories(parentCategoryId.get(), pageable).map(CategoryDto::new);
        } else {
            log.debug("Getting All categories.");
            categoryDtoPage = categoryService.getCategories(pageable).map(CategoryDto::new);
        }
        return ResponseEntity.ok(categoryDtoPage);
    }

    @ApiOperation("Retrieve a category by the identifier.")
    @GetMapping("/{categoryId}")
    public ResponseEntity<CategoryDto> getCategoryById(@PathVariable Long categoryId) {
        Optional<CategoryDto> optionalCategory = categoryService.getCategory(categoryId).map(CategoryDto::new);
        log.debug("Getting the category with Id : " + categoryId);
        if (optionalCategory.isPresent()) {
            return ResponseEntity.ok(optionalCategory.get());
        }
        throw new CategoryNotFountException();
    }

    @ApiOperation("Create a Category.")
    @PostMapping
    public ResponseEntity<CategoryDto> createCategory(@Valid @RequestBody CreateCategoryDto categoryDto) {
        log.debug("Creating a new category with name : " + categoryDto.getName());
        CategoryDto returnedCategoryDto = new CategoryDto(categoryService.createCategory(modelMapper.map(categoryDto, Category.class)));
        return ResponseEntity.status(HttpStatus.CREATED).body(returnedCategoryDto);
    }

    @ApiOperation("Update the Category characteristics.")
    @PutMapping
    public ResponseEntity<CategoryDto> updateCategory(@Valid @RequestBody CategoryDto categoryDto) {
        Optional<CategoryDto> optionalCategory = categoryService.getCategory(categoryDto.getId()).map(CategoryDto::new);
        log.debug("Updating the category with Id : " + categoryDto.getId());
        if (optionalCategory.isPresent()) {
            Category category = categoryService.updateCategory(modelMapper.map(categoryDto, Category.class));
            return ResponseEntity.ok(new CategoryDto(category));
        }

        log.debug("the category with Id : " + categoryDto.getId() + " is missed.");
        throw new CategoryNotFountException();
    }
}
