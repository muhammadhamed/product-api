package com.rakuten.product.service;

import java.math.BigDecimal;

public interface CurrencyConverterService {

    BigDecimal getRate(String targetCurrency);

    BigDecimal convertToEuro(String sourceCurrency, BigDecimal amount);
}
