package com.rakuten.product.service;

import com.rakuten.product.model.Product;
import com.rakuten.product.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Slf4j
@Service
public class ProductService {

    private ProductRepository productRepository;

    private CurrencyConverterService currencyConverterService;

    @Autowired
    public ProductService(ProductRepository productRepository, CurrencyConverterService currencyConverterService) {
        this.productRepository = productRepository;
        this.currencyConverterService = currencyConverterService;
    }

    public Page<Product> getProducts(Pageable pageable) {
        Page<Product> products = productRepository.findAll(pageable);
        return products;
    }

    public Page<Product> getProductsByCatergoryId(Long categoryId, Pageable pageable) {
        Page<Product> products = productRepository.findProductsByCategoryId(categoryId, pageable);
        return products;
    }

    public Optional<Product> getProduct(Long productId) {
        Optional<Product> optionalProduct = productRepository.findById(productId);
        return optionalProduct;
    }

    public Product createProduct(Product product) {
        // Convert the price to the Default currency "EUR".
        convertPriceToDefualtCurrency(product);
        Product createdProduct = productRepository.save(product);
        return createdProduct;
    }

    public Product updateProduct(Product product) {
        // Convert the price to the Default currency "EUR".
        convertPriceToDefualtCurrency(product);
        Product updateProduct = productRepository.save(product);
        return updateProduct;
    }

    public void deleteProduct(Product product) {
        productRepository.delete(product);
    }

    private void convertPriceToDefualtCurrency(Product product) {
        // Convert the price to the Default currency "EUR".
        final String currecyCode = product.getCurrencyCode();
        if (currecyCode != null && !"EUR".equalsIgnoreCase(currecyCode)) {
            log.debug("Product id : " + product.getId() + " need price conversion, With currency : " + product.getPrice() + " " + currecyCode + ".");
            BigDecimal price = currencyConverterService.convertToEuro(currecyCode, product.getPrice());
            product.setPrice(price);
            product.setCurrencyCode("EUR");
            log.debug("Product id : " + product.getId() + ", With currency conversion : " + product.getPrice() + " EUR.");
        }
    }
}