package com.rakuten.product.service;

import com.rakuten.product.model.CurrencyRates;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Slf4j
@Service
public class FixerCurrencyConverterService implements CurrencyConverterService {

    @Value("${service.fixer.url}")
    private String baseUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override

    public BigDecimal getRate(String targetCurrency) {
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<CurrencyRates> responseEntity = restTemplate.exchange(baseUrl, HttpMethod.GET, new HttpEntity<Object>(headers), CurrencyRates.class);
        log.info("Response header" + responseEntity.getHeaders());

        CurrencyRates currencyRates = responseEntity.getBody();
        if (currencyRates != null) {
            BigDecimal rate = currencyRates.getRates().get(targetCurrency.toUpperCase());
            log.info("Rate for " + targetCurrency + " is " + rate + " EUR.");
            return rate;
        }
        throw new IllegalStateException();
    }

    @Override
    public BigDecimal convertToEuro(String sourceCurrency, BigDecimal amount) {
        return amount.divide(getRate(sourceCurrency), 2, RoundingMode.HALF_UP);
    }
}
