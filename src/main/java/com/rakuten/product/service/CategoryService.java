package com.rakuten.product.service;

import com.rakuten.product.model.Category;
import com.rakuten.product.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Optional<Category> getCategory(Long categoryId) {
        Optional<Category> optionalCategory = categoryRepository.findById(categoryId);
        return optionalCategory;
    }

    public Page<Category> getCategories(Pageable pageable) {
        Page<Category> categories = categoryRepository.findAll(pageable);
        return categories;
    }

    public Page<Category> getCategories(Long parentCategoryId, Pageable pageable) {
        Page<Category> categories = categoryRepository.findChildCategoriesByparentCategoryId(parentCategoryId, pageable);
        return categories;
    }

    public Category createCategory(Category category) {
        Category createdCategory = categoryRepository.save(category);
        return createdCategory;
    }

    public Category updateCategory(Category category) {
        Category updatedCategory = categoryRepository.save(category);
        return updatedCategory;
    }
}
