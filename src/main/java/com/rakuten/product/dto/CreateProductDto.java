package com.rakuten.product.dto;

import com.rakuten.product.model.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class CreateProductDto {

    @NotEmpty
    @ApiModelProperty(notes = "The Product name.")
    private String name;

    @ApiModelProperty(notes = "The Product description.")
    private String description;

    @PositiveOrZero
    @ApiModelProperty(notes = "The Product price.")
    private BigDecimal price;

    @ApiModelProperty(notes = "The Product currency code in case of using non Euro, ex: USD.")
    private String currencyCode;

    @NotNull
    @ApiModelProperty(notes = "The Product Category.")
    private CategoryDto category;

    public CreateProductDto(Product product) {
        if (product != null) {
            this.name = product.getName();
            this.description = product.getDescription();
            this.price = product.getPrice();
            this.currencyCode = product.getCurrencyCode();
            this.category = new CategoryDto(product.getCategory());
        }
    }
}
