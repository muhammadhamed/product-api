package com.rakuten.product.dto;

import com.rakuten.product.model.Category;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class CreateCategoryDto {

    @NotEmpty
    @Size(min = 2)
    @ApiModelProperty("The Category name.")
    private String name;

    @ApiModelProperty("The Category description.")
    private String description;

    @ApiModelProperty("The Category parent category, non if it's a root category.")
    private CategoryDto parentCategory;

    public CreateCategoryDto(Category category) {
        if (category != null) {
            this.name = category.getName();
            this.description = category.getDescription();
            if (category.getParentCategory() != null) {
                this.parentCategory = new CategoryDto(category.getParentCategory());
            }
        }
    }
}
