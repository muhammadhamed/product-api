package com.rakuten.product.model;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Map;

@Data
public class CurrencyRates {
    private Boolean success;
    private Timestamp timestamp;
    private String base;
    private LocalDate date;
    private Map<String, BigDecimal> rates;
}
