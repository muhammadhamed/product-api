package com.rakuten.product.repository;

import com.rakuten.product.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {
    Page<Category> findChildCategoriesByparentCategoryId(Long parentCategoryId, Pageable pageable);
}
