# Rakuten Product API

Rakuten DE is moving towards the distributed, REST API-driven world.
As such, one of the main API’s we’ll be having is the Product API.

## Prototype of our Product API support:

 - Product management (Create, Read, Update, and Delete product). 
 - Product category management ( Create, Read, Update category).
 - API supports sorting and paging as **Rakuten** have a big product list.
 - Product standard price in **EUR** but supports other currencies as well, 
   with a real time conversion.

## Assumptions 
 - Rakuten customers demand is variant depending on the offers, seasons.
  So the API need to be elastic scale up and down based on the demand.
 - This API is used with parteners so we do nod provide remove for the categories information.
 - Rakuten has on-premises private cloud, but need the solution to be portable to be migrated 
 to the public one. 
 
## Technologies
 - Java 8.
 - Spring Boot. 
 - MySQL (For out MVP we use H2).
 - Docker.
 - Swagger.
 - Kubernetes (Helm).

# Solution Architecture

## Architecture Selection

In our project the business is highly dynamic. Also the business scaling is promising. So we recommend that 
the application will be cloud compliant. The selection of the architecture here will be a **Microservice Architecture** it
will be optimal for this business agility. The load on the application endpoints is not close. The service will be 
hitted more that the customer search so scaling this endpoint only will be cost saving compared to replicating the whole application. 
Also the give the flexibility to add new small and independent components to enrich the customer business.
 
 ## CI/CD
  
 gitlab-ci is used to achieve:
   - Build the application.
   - Publish the Docker image.
   - Deploy it to the GKE. 
    
  *) Travis-ci will automatically run once you push a change to this github repository.
    
 ## Static Code Analysis
  
I've integrated gitlab-ci and travis-ci  to SonarCloud to publish the Code analysis for 
   - **Product API**
     SonarCloud public link : https://sonarcloud.io/dashboard?id=com.rakuten%3Aproduct 
         
      ![Code quality](https://sonarcloud.io/api/project_badges/measure?project=com.rakuten%3Aproduct&metric=alert_status "Code Quality") ![Code Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.rakuten%3Aproduct&metric=coverage "Code Coverage") ![Code Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.rakuten%3Aproduct&metric=sqale_rating "Code Coverage") ![Loc](https://sonarcloud.io/api/project_badges/measure?project=com.rakuten%3Aproduct&metric=ncloc "Lines of Code") ![Code duplication](https://sonarcloud.io/api/project_badges/measure?project=com.rakuten%3Aproduct&metric=duplicated_lines_density) ![Vul](https://sonarcloud.io/api/project_badges/measure?project=com.rakuten%3Aproduct&metric=vulnerabilities)
      
 ## Deployment
 
 The deployment done on Google Kubernetes Engine.
 
 ### using Helm 

Here's how we deploy it.
 ``` shelll
 helm upgrade --install  product ./helm/product
 ```
 
 